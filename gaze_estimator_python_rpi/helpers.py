import numpy as np


def fit_line(coordinates):
    x_coordinates, y_coordinates = zip(*coordinates)

    # Fit a line through the coordinates using linear regression
    coefficients = np.polyfit(x_coordinates, y_coordinates, 1)

    # Get the slope and y-intercept of the line
    slope = coefficients[0]
    y_intercept = coefficients[1]

    # Calculate the x values for the beginning and ending points
    x_begin = min(x_coordinates)
    x_end = max(x_coordinates)

    # Calculate the corresponding y values for the beginning and ending points
    y_begin = slope * x_begin + y_intercept
    y_end = slope * x_end + y_intercept

    return (x_begin, y_begin), (x_end, y_end)


def convert_to_image_space(image_shape, point):
    image_rows, image_cols, _ = image_shape
    x, y = point[:2]
    x_im = np.int32(x * image_cols)
    y_im = np.int32(y * image_rows)

    return x_im, y_im


def landmark2list(landmark) -> list:
    return [landmark.x, landmark.y, landmark.z]


def isRotationMatrix(R):
    # Checks if a matrix is a valid rotation matrix.
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6