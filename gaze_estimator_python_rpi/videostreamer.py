from PyQt5.QtCore import Qt, QSettings, QThread, QTimer, QObject, pyqtSlot, pyqtSignal
import cv2
import datetime
import numpy as np
# from PyQt5 import QtWidgets
# from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QApplication, QWidget
# from picamera2.previews.qt import QGlPicamera2
from checkOS import is_raspberry_pi

if is_raspberry_pi():
    from picamera2 import Picamera2, Preview


class FPS:
    def __init__(self):
        # store the start time, end time, and total number of frames
        # that were examined between the start and end intervals
        self._start = None
        self._end = None
        self._numFrames = 0

    def start(self):
        # start the timer
        self._start = datetime.datetime.now()
        return self

    def stop(self):
        # stop the timer
        self._end = datetime.datetime.now()

    def update(self):
        # increment the total number of frames examined during the
        # start and end intervals
        self._numFrames += 1

    def elapsed(self):
        # return the total number of seconds between the start and
        # end interval
        return (self._end - self._start).total_seconds()

    def fps(self):
        # compute the (approximate) frames per second
        return self._numFrames / self.elapsed()


class VideoStreamer(QObject):
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    frame = pyqtSignal(np.ndarray)

    def __init__(self, settings: QSettings = None):
        super().__init__()
        # warnings.filterwarnings('default', category=DeprecationWarning)
        self.fps = None
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        if is_raspberry_pi():
            self.picam2 = Picamera2()
            config = self.picam2.create_preview_configuration()
            self.picam2.configure(config)
            #         self.picam2.start_preview(Preview.QTGL)
            #         self.preview = self.picam2.preview
            self.picam2.start()
        else:
            self.capture_stream = cv2.VideoCapture(0)
            self.capture_stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
        
        # Update the camera feed at intervals
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.updateImage)
        self.timer.start(10)  # Update every 10 ms

        print(f"{self.__class__.__name__}: info; initialized")
# 
#     @pyqtSlot()
#     def init_stream(self):
#         # start the thread
#         self.picam2.start()
#         self.start(QThread.HighPriority)
#         print(f"{self.__class__.__name__}: info; video stream started")
# 
#     @pyqtSlot()
#     def run(self):
#         try:
#             self.fps = FPS().start()
#             while True: # self.capture_stream.isOpened():
#                 image = self.picam2.capture_array()
# #                 success, image = self.capture_stream.read()
# #                 if not success:
# #                     self.postMessage.emit(f"{self.__class__.__name__}: error; ignoring empty camera frame")
# #                     continue
#                 self.frame.emit(image)
#                 self.fps.update()
#         except Exception as err:
#             self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
#         finally:
#             self.finished.emit()
    def updateImage(self):
        # Get the current frame
        if is_raspberry_pi():
            image = self.picam2.capture_array()
        else:
            success, image = self.capture_stream.read()
        if image is not None:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            
            # Convert the frame to QImage and then to QPixmap
#             image = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
#             pix = QPixmap.fromImage(image)
            # Display the QPixmap in the label
#             self.label.setPixmap(pix)
            self.frame.emit(image)
        
    @pyqtSlot()
    def stop(self):
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopping")
        self.picam2.stop_preview()
# 
#         try:
#             if self.isRunning():
#                 self.requestInterruption()
# #             self.capture_stream.release()
#             self.fps.stop()
#             self.print(f"{self.__class__.__name__}: "
#                        f"info; finished, approx. processing speed: {self.fps.fps()} fps")
#         except Exception as err:
#             self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
#         finally:
#             # self.camera.close()  # results in error: 'output port couldn't be disabled'?
#             self.quit()  # Note that thread quit is required, otherwise strange things happen.
#             print(f"{self.__class__.__name__}: info; stopped")
