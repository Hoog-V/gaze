import cv2
from PyQt5.QtCore import QSettings, QObject, pyqtSlot, pyqtSignal
import numpy as np
from helpers import convert_to_image_space, fit_line, landmark2list
from smoothers import IIR
from math import atan2, pi
from mediapipe.tasks.python import BaseOptions
from mediapipe.tasks.python.vision import FaceLandmarker, FaceLandmarkerResult, FaceLandmarkerOptions, RunningMode


def get_left_eye_RoI(image, face_landmarks, roll):
    roi_factor = .75
    frame_height, frame_width = image.shape[:2]
    eye_lid_x = [face_landmarks[33].x, face_landmarks[133].x]
    eye_lid_y = [face_landmarks[33].y, face_landmarks[133].y, face_landmarks[145].y, face_landmarks[159].y]
    centre_x, centre_y = (0.5 * (min(eye_lid_x) + max(eye_lid_x)), 0.5 * (min(eye_lid_y) + max(eye_lid_y)))
    RoI_x_1, RoI_y_1 = (centre_x - roi_factor * (max(eye_lid_x) - min(eye_lid_x)),
                        centre_y - roi_factor * (max(eye_lid_y) - min(eye_lid_y)))
    RoI_x_2, RoI_y_2 = (centre_x + roi_factor * (max(eye_lid_x) - min(eye_lid_x)),
                        centre_y + roi_factor * (max(eye_lid_y) - min(eye_lid_y)))
    x1, y1 = max(np.int32(RoI_x_1 * frame_width), 0), max(np.int32(RoI_y_1 * frame_height), 0)
    x2, y2 = min(np.int32(RoI_x_2 * frame_width), frame_width), min(np.int32(RoI_y_2 * frame_height), frame_height)
    RoI = np.copy(image[y1:y2, x1:x2, :])
    RoI_height, RoI_width = RoI.shape[:2]
    rotate_matrix_2D = cv2.getRotationMatrix2D(center=(RoI_width / 2, RoI_height / 2), angle=roll, scale=1)
    RoI = cv2.warpAffine(RoI, rotate_matrix_2D, dsize=(RoI_width, RoI_height))

    return RoI


def get_gaze_direction(image, face_landmarks):
    left_iris_centre_co = 468
    left_iris_indices = [469, 470, 471, 472]
    # palpebral fissure heigth (PFH) from distance between landmarks 145, 159
    # palpebral fissure width (PFW) from distance between landmarks 33, 133

    # left_eye_lid_indices = [33, 7, 163, 144, 145, 153, 154, 155, 133, 173, 157, 158, 159, 160, 161, 246]
    # left_eye_lid_indices = [163, 144, 145, 153, 154, 157, 158, 159, 160, 161]
    left_eye_lid_indices = [144, 145, 153, 158, 159, 160]

    # find palpebral fissure horizontal
    left_eye_id_coordinates = []
    for index in left_eye_lid_indices:
        left_eye_id_coordinates.append((face_landmarks[index].x, face_landmarks[index].y))
    (x_begin, y_begin), (x_end, y_end) = fit_line(left_eye_id_coordinates)
    # Note that this roll estimate also includes the angle of the palpebral fissure horizontal, which is person-specific
    palpebral_fissure_roll = (180 / pi) * atan2(y_end - y_begin, x_end - x_begin)

    # print(f"{roll}")
    # todo: continuously measure length of palpebral fissure as estimate of eyeball diameter,
    #  only when head is viewed frontally
    # zijn er ook andere parameters dynamisch te calibreren? zoals bv offset eye_centre to iris_centre
    #  of bv. maximale yaw-pitch die met een oog uberhapt mogelijk is
    #  gaat waarschijnlijk niet lukken, want dit is ook een functie van de afstand tot de camera....
    # tan yaw = eye_centre to iris_centre horizontal distance over eyeball radius
    # tan pitch =eye_centre to iris_centre vertical distance over eyeball radius
    #  compensated for roll?

    left_eye_centre = [0.5 * (x_begin + x_end), 0.5 * (y_begin + y_end)]
    left_iris_centre = landmark2list(face_landmarks[left_iris_centre_co])[:2]
    # image_rows, image_cols, _ = image.shape
    left_iris_width = 0.5 * abs(face_landmarks[left_iris_indices[0]].x - face_landmarks[left_iris_indices[2]].x)

    point1_im = convert_to_image_space(image.shape,
                                       [face_landmarks[left_iris_indices[0]].x, face_landmarks[left_iris_indices[0]].y])
    point2_im = convert_to_image_space(image.shape,
                                       [face_landmarks[left_iris_indices[2]].x, face_landmarks[left_iris_indices[2]].y])
    cv2.line(image, point1_im, point2_im, (0, 0, 255), 1)

    # point1_im = convert_to_image_space(image.shape, [face_landmarks[left_iris_indices[1]].x, face_landmarks[left_iris_indices[1]].y])
    # point2_im = convert_to_image_space(image.shape, [face_landmarks[left_iris_indices[3]].x, face_landmarks[left_iris_indices[3]].y])
    # cv2.line(image, point1_im, point2_im, (0, 255, 0), 1)

    point1_im = convert_to_image_space(image.shape, (x_begin, y_begin))
    point2_im = convert_to_image_space(image.shape, (x_end, y_end))
    cv2.line(image, point1_im, point2_im, (255, 0, 0), 1)

    eye_centre_im = convert_to_image_space(image.shape, left_eye_centre)
    iris_centre_im = convert_to_image_space(image.shape, left_iris_centre)
    cv2.line(image, eye_centre_im, iris_centre_im, (255, 255, 255), 1)

    iris_horizontal_lengh = (face_landmarks[left_iris_indices[0]].x - face_landmarks[left_iris_indices[2]].x) ** 2 + \
                            (face_landmarks[left_iris_indices[0]].y - face_landmarks[left_iris_indices[2]].y) ** 2
    iris_vertical_lengh = (face_landmarks[left_iris_indices[1]].x - face_landmarks[left_iris_indices[3]].x) ** 2 + \
                          (face_landmarks[left_iris_indices[1]].y - face_landmarks[left_iris_indices[3]].y) ** 2
    # print(f"{iris_horizontal_lengh/iris_vertical_lengh}")

    return image


class GazeDirectionEstimator(QObject):
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    result = pyqtSignal(np.ndarray, FaceLandmarkerResult, list, list)

    def __init__(self, settings: QSettings = None):
        super().__init__()
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        self.decimals = self.settings.value('decimals', 1, int)
        self.face_nr = 0  # if there are multiple, pick first
        self.model_path = self.settings.value('Landmarker/model_path', None, type=str)
        self.options = FaceLandmarkerOptions(
            base_options=BaseOptions(model_asset_path=self.model_path),
            output_face_blendshapes=False,
            output_facial_transformation_matrixes=True,
            num_faces=1,
            running_mode=RunningMode.VIDEO)
        self.detector = FaceLandmarker.create_from_options(self.options)
        self.alpha = self.settings.value('GazeDirectionEstimator/alpha', 1, float)
        self.max_delta_yaw = self.settings.value('GazeDirectionEstimator/max_delta_yaw', 30, float)
        self.max_delta_pitch = self.settings.value('GazeDirectionEstimator/max_delta_pitch', 30, float)
        self.yaw_offset = self.settings.value('GazeDirectionEstimator/jaw_offset', 0, float)
        self.pitch_offset = self.settings.value('GazeDirectionEstimator/pitch_offset', 0, float)
        self.angleSmoothers = [IIR(self.alpha), IIR(self.alpha), IIR(self.alpha)]
        print(self.alpha)
        print(f"{self.__class__.__name__}: info; initialized.")

    @pyqtSlot(np.ndarray, FaceLandmarkerResult, list)
    def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None,
               head_rotation: list = None):
        if image is None:
            return
        if detection_results is None or len(detection_results.face_landmarks) == 0:
            return
        if head_rotation is None or len(head_rotation) == 0:
            return

        face_landmarks = detection_results.face_landmarks[self.face_nr]
        # ################
        # # temporary code for analyzing out geometries
        # image = get_gaze_direction(image, face_landmarks)
        #
        # yaw, pitch, roll = head_rotation
        # left_eye_RoI = get_left_eye_RoI(image, face_landmarks, roll)
        # cv2.imshow('left_eye_RoI', cv2.resize(left_eye_RoI, None, fx=10, fy=10, interpolation=cv2.INTER_CUBIC))
        # #################

        #
        # left_eye_landmarks = face_landmarks[263:374]
        # gaze_direction_vector = self.detector._calculate_gaze(left_eye_landmarks)
        # print("Gaze Direction Vector:", gaze_direction_vector)

        # find out which eye is most visible and use that to determine gaze direction
        left_palpebral_fissure_width = np.sqrt((face_landmarks[33].x - face_landmarks[133].x) ** 2
                                               - (face_landmarks[33].y - face_landmarks[133].y) ** 2)
        right_palpebral_fissure_width = np.sqrt((face_landmarks[362].x - face_landmarks[263].x) ** 2
                                                - (face_landmarks[362].y - face_landmarks[263].y) ** 2)

        # Use iris and a few eyelid coordinates
        if left_palpebral_fissure_width > right_palpebral_fissure_width:
            iris_centre_co = 468
            iris_indices = [469, 470, 471, 472]
            eye_lid_indices = [144, 145, 153, 158, 159, 160]
            eye_lid_indices = [33, 7, 163, 144, 145, 153, 154, 155, 133, 173, 157, 158, 159, 160, 161, 246]
        # left_eye_lid_indices = [163, 144, 145, 153, 154, 157, 158, 159, 160, 161]
            palpebral_fissure_width = left_palpebral_fissure_width
        else:
            iris_centre_co = 473
            iris_indices = [474, 475, 476, 477]
            eye_lid_indices = [373, 374, 380, 385, 386, 387]
            palpebral_fissure_width = right_palpebral_fissure_width

        eye_id_coordinates = []
        for index in eye_lid_indices:
            eye_id_coordinates.append((face_landmarks[index].x, face_landmarks[index].y))
        (x_begin, y_begin), (x_end, y_end) = fit_line(eye_id_coordinates)
        eye_centre = [0.5 * (x_begin + x_end), 0.5 * (y_begin + y_end)]
        iris_centre = landmark2list(face_landmarks[iris_centre_co])[:2]
        iris_width = abs(face_landmarks[iris_indices[0]].x - face_landmarks[iris_indices[2]].x)
        iris_heigth = abs(face_landmarks[iris_indices[1]].y - face_landmarks[iris_indices[3]].y)
        yaw = round(self.max_delta_yaw * (eye_centre[0] - iris_centre[0]) / (iris_width / 2),
                    self.decimals)
        pitch = round(self.max_delta_pitch * (eye_centre[1] - iris_centre[1]) / (iris_heigth / 2),
                      self.decimals)
        yaw -= (head_rotation[0] + self.yaw_offset)
        pitch -= (head_rotation[1] + self.pitch_offset)
        roll = head_rotation[2]

        yaw, _ = self.angleSmoothers[0].process(yaw)
        pitch, _ = self.angleSmoothers[1].process(pitch)
        roll, _ = self.angleSmoothers[2].process(roll)
        yaw = round(yaw, self.decimals)
        pitch = round(pitch, self.decimals)
        roll = round(roll, self.decimals)

        self.postMessage.emit(f"{self.__class__.__name__}: info; "
                              f"head orientation: yaw={head_rotation[0]:2.1f}°, pitch={head_rotation[1]:2.1f}°, roll={head_rotation[2]:2.1f}°"
                              f"gaze direction: yaw={yaw:2.1f}°, pitch={pitch:2.1f}°, roll={roll:2.1f}°")

        self.result.emit(image, detection_results, head_rotation, [yaw, pitch, roll])

    @pyqtSlot()
    def stop(self):
        try:
            pass
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")
