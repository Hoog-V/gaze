import json
import numpy as np
import socket
from PyQt5.QtCore import QObject, QSettings, pyqtSignal, pyqtSlot, QThread


class ClientThread(QThread):
    received = pyqtSignal(str)
    postMessage = pyqtSignal(str)
    disconnected = pyqtSignal()

    def __init__(self, client_socket, address, parent=None):
        super().__init__(parent)
        self.clientSocket = client_socket
        self.address = address
        self.running = True

    def run(self):
        while self.running:
            try:
                message = self.clientSocket.recv(1024)
                if message:
                    self.received.emit(message.decode('utf-8'))
                else:  # No message means the client has disconnected
                    break
            except ConnectionResetError:
                self.postMessage.emit(f"{self.__class__.__name__}: info; client disconnected: {self.address}")
                print(f"{self.__class__.__name__}: info; client disconnected: {self.address}")
                print('ConnectionResetError')
                break
            except OSError as e:
                if e.winerror == 10038:  # Socket operation on non-socket
                    print('Error Socket operation on non-socket')
                    break  # Exit the loop if the socket is invalid
                print(f"Unexpected OSError: {e}")
            except Exception as e:
                print(e)
                self.postMessage.emit(f"{self.__class__.__name__}: error; accepting connections: {e}")
                break
        self.cleanup()

    def cleanup(self):
        if self.clientSocket:
            self.clientSocket.close()
            self.clientSocket = None
        self.disconnected.emit()

    def stop(self):
        self.running = False


class ServerThread(QThread):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent

    def run(self):
        self.parent.acceptConnections()


class TCPServer(QObject):
    clientConnected = pyqtSignal(str)
    clientDisconnected = pyqtSignal(str)
    postMessage = pyqtSignal(str)

    def __init__(self, my_settings: QSettings):
        super().__init__()
        self.server_thread = None
        self.server = None
        self.settings = my_settings
        print(f"{self.__class__.__name__}: info; loading settings from {self.settings.fileName()}")
        self.host = self.settings.value('TCPserver/host', '0.0.0.0', str)
        self.port = self.settings.value('TCPserver/port', 5678, int)
        self.client_threads = []
        self.startServer()

    def startServer(self):
        # Initialize the server socket
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((self.host, self.port))
        self.server.listen()

        # Initialize and start the server thread
        self.server_thread = ServerThread(self)
        self.server_thread.start()

    def acceptConnections(self):
        while True:
            try:
                client, address = self.server.accept()
                client_thread = ClientThread(client, address)
                client_thread.received.connect(self.handleReceived)
                client_thread.disconnected.connect(lambda: self.removeClient(client_thread))
                client_thread.postMessage.connect(self.postMessage)
                client_thread.start()
                self.client_threads.append(client_thread)
                self.postMessage.emit(f"{self.__class__.__name__}: info; client connected: {address}")
                print(f"{self.__class__.__name__}: info; client connected: {address}")
            except Exception as e:
                self.postMessage.emit(f"{self.__class__.__name__}: error; accepting connections: {e}")

    def handleReceived(self, message):
        self.postMessage.emit(f"{self.__class__.__name__}: info; received {message}")

    def removeClient(self, client_thread):
        if client_thread in self.client_threads:
            self.client_threads.remove(client_thread)
            client_thread.quit()
            client_thread.wait()

    @pyqtSlot(str)
    def sendMessage(self, message):
        for client_thread in self.client_threads:
            if client_thread.clientSocket is None:
                break
            try:
                client_thread.clientSocket.sendall(message.encode('utf-8'))
            except ConnectionAbortedError:
                print("Connection aborted error while sending message")
                # Handle cleanup or reconnection logic here
            except Exception as e:
                print(f"Unexpected error: {e}")
                # Additional error handling

    @pyqtSlot(np.ndarray, object, list, list)
    def update(self, image: np.ndarray = None, detection_results: object = None,
               head_rotation: list = None, gaze_direction: list = None):
        data = {
            "head_rotation": head_rotation,
            "gaze_direction": gaze_direction
        }
        message = json.dumps(data)
        self.sendMessage(message)

    def closeServer(self):
        for clientThread in self.client_threads:
            clientThread.stop()
            clientThread.wait()
        self.acceptThread.quit()
        self.acceptThread.wait()
        # self.server.close()

    def stop(self):
        self.closeServer()
        self.postMessage.emit(f"{self.__class__.__name__}: info; stopped")
