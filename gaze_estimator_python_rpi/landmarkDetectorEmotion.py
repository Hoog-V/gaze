from PyQt5.QtCore import Qt, QSettings, QObject, pyqtSlot, pyqtSignal
import numpy as np
import mediapipe as mp
from mediapipe.tasks.python import BaseOptions
from mediapipe.tasks.python.vision import FaceLandmarker, FaceLandmarkerResult, FaceLandmarkerOptions, RunningMode
import time
from smoothers import IIR
import tensorflow as tf

NR_OF_LANDMARKS = 478

# model = tf.keras.models.load_model('\LSTM_model_73%_test_acc')  # \LSTM_model_abit_optimized')
model = tf.keras.layers.TFSMLayer('\LSTM_model_73%_test_acc', call_endpoint='serving_default')
blendS_to_print = ['1', '2', '3', '4', '5', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '25', '30', '34', '35', '44', '45', '46', '47', '48']


class LandmarkDetector(QObject):
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    result = pyqtSignal(np.ndarray, FaceLandmarkerResult)

    def __init__(self, settings: QSettings = None):
        super().__init__()
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        self.decimals = self.settings.value('decimals', 1, int)
        self.model_path = self.settings.value('Landmarker/model_path', None, type=str)
        self.options = FaceLandmarkerOptions(
            base_options=BaseOptions(model_asset_path=self.model_path),
            output_face_blendshapes=True,
            output_facial_transformation_matrixes=True,
            num_faces=1,
            min_face_detection_confidence=.1,
            min_tracking_confidence=.1,
            running_mode=RunningMode.VIDEO)
        self.detector = FaceLandmarker.create_from_options(self.options)
        self.face_nr = 0  # if there are multiple, pick first
        self.alpha = 0.9
        self.landmarkSmoothers = []
        for index in range(0, NR_OF_LANDMARKS):
            self.landmarkSmoothers.append([IIR(self.alpha), IIR(self.alpha), IIR(self.alpha)])
        print(f"{self.__class__.__name__}: info; initialized. Media pipe version: {mp.__version__}")

    @pyqtSlot(np.ndarray)
    def update(self, image: np.ndarray = None):
        if image is not None:  # we have a new image
            mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=image)
            frame_timestamp_ms = round(time.time() * 1000)
            detection_results = self.detector.detect_for_video(mp_image, frame_timestamp_ms)
            if detection_results is not None and len(detection_results.face_landmarks) > 0:
                face_landmarks = detection_results.face_landmarks[self.face_nr]
                blendshapes = detection_results.face_blendshapes[0]
                blendshapes = np.array(blendshapes)
                blendy = [blend.score  for blend in blendshapes[:] if str(blend.index) in blendS_to_print]
                blendy = np.array(blendy, dtype=np.float64)
                blendy = np.reshape(blendy, (1, 26,1))
                y_pred = model.predict(blendy)
                if round(float(y_pred)) == 3:
                    print("Happy")
                elif round(float(y_pred)) == 4:
                    print("Sad")
                for index, face_landmark in enumerate(face_landmarks):
                    face_landmark = face_landmarks[index]
                    face_landmark.x, _ = self.landmarkSmoothers[index][0].process(face_landmark.x)
                    face_landmark.y, _ = self.landmarkSmoothers[index][1].process(face_landmark.y)
                    face_landmark.z, _ = self.landmarkSmoothers[index][2].process(face_landmark.z)
                    face_landmark.x = round(face_landmark.x, self.decimals)
                    face_landmark.y = round(face_landmark.y, self.decimals)
                    face_landmark.z = round(face_landmark.z, self.decimals)
                self.result.emit(image, detection_results)

    @pyqtSlot()
    def stop(self):
        try:
            pass
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

