from PyQt5.QtCore import QObject, QSettings, pyqtSignal, pyqtSlot, QThreadPool, QRunnable
from mediapipe.tasks.python.vision import FaceLandmarkerResult
import numpy as np
from os import path
from pathlib import Path
from datetime import datetime
from csv import writer as csv_writer
from helpers import landmark2list
from time import time


class DataWriter(QObject):
    postMessage = pyqtSignal(str)

    def __init__(self, my_settings: QSettings):
        super().__init__()

        self.prev_time = None
        self.recording_flag = None
        self.csv_file_name = None
        self.settings = my_settings
        print(f"{self.__class__.__name__}: info; loading settings from {self.settings.fileName()}")
        self.decimals = self.settings.value('decimals', 1, int)
        self.store_data_flag = self.settings.value('Recording/store_data', False, bool)
        self.Ts = 1.0 / float(self.settings.value('Recording/fps', 1, int))
        storage_path = self.settings.value('Recording/data_folder', '', str)
        self.storage_path = path.sep.join([storage_path, self.settings.value('name')])
        if not path.exists(self.storage_path):
            Path(self.storage_path).mkdir(parents=True, exist_ok=True)
        self.header_written = False
        self.face_nr = 0  # if there are multiple, pick first
        print(f"{self.__class__.__name__}: info; started, local storage is {self.storage_path}.")

    @pyqtSlot(bool)
    def toggle(self, state: bool):
        self.start() if state else self.stop()

    @pyqtSlot()
    def start(self):
        if self.store_data_flag:
            self.postMessage.emit(f"{self.__class__.__name__}: info; start recording")
            self.csv_file_name = path.sep.join([self.storage_path, f"{datetime.now().strftime('%y%m%d_%H%M%S')}.csv"])
            self.recording_flag = True
            self.prev_time = None
            self.header_written = False

    @pyqtSlot()
    def stop(self):
        if self.recording_flag:
            self.recording_flag = False
            self.postMessage.emit(f"{self.__class__.__name__}: info; stop recording")

    @pyqtSlot(np.ndarray, FaceLandmarkerResult, list, list)
    def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None,
               head_rotation: list = None, gaze_direction: list = None):
        if image is None:
            return
        if detection_results is None or len(detection_results.face_landmarks) == 0:
            return
        if head_rotation is None or len(head_rotation) == 0:
            return
        if gaze_direction is None or len(gaze_direction) == 0:
            return
        if not self.recording_flag:
            return

        face_landmarks = detection_results.face_landmarks[self.face_nr]
        if not self.header_written:
            header = ['time_delta']
            header += [f'face_landmark_{i}_{j}' for i in range(0, len(face_landmarks)) for j in ['x', 'y', 'z']]
            header += [f'head_{i}' for i in ['yaw', 'pitch', 'roll']]  # for j in ['x', 'y', 'z']]
            header += [f'gaze_{i}' for i in ['yaw', 'pitch', 'roll']]  # for j in ['x', 'y', 'z']]
            with open(self.csv_file_name, 'a+', newline='') as csv_file:
                data_writer = csv_writer(csv_file, delimiter=',', quotechar='|')
                data_writer.writerow(header)
                self.header_written = True

        if self.prev_time is None or time() - self.prev_time > self.Ts:
            row = [0] if self.prev_time is None else [round(time() - self.prev_time, self.decimals)]
            for face_landmark in face_landmarks:
                row += landmark2list(face_landmark)
            row += head_rotation
            row += gaze_direction

            with open(self.csv_file_name, 'a+', newline='') as csv_file:
                data_writer = csv_writer(csv_file, delimiter=',', quotechar='|')
                data_writer.writerow(row)

            self.prev_time = time()
