from numpy import zeros_like, arange
from collections import deque
from scipy import signal


class Smoother(object):
    """
    Smooth a data sequence, x
    """

    def __init__(self, alpha, beta, window_len):
        super().__init__()
        self.alpha = alpha
        self.beta = beta
        self.window_len = window_len
        self.deq = deque(maxlen=window_len) if window_len > 0 else None
        self.t = arange(0, window_len) if window_len > 0 else None
        self.initialized = False

    def reset(self):
        if self.deq is not None:
            self.deq.clear()
        self.initialized = False

    def process(self, x):
        pass

    def eval(self, x):
        if not self.initialized:
            return

        y = zeros_like(x)
        for i, x_ in enumerate(x):
            y[i] = self.process(x_)

        return y


class Biquad(object):
    """
    Filter a data sequence, x, using an IIR filter defined by a second-order section representation, sos.
    The second-order filter coefficients must have length 6, where
    the first three elements provide the numerator coefficients and the last three providing the denominator coefficients.
    """

    def __init__(self, sos):
        super().__init__()
        if len(sos) != 6:
            raise ValueError('Wrong number of coefficients')
        self.sos = sos
        self.w = 2 * [0]
        self.initialized = False

    def process(self, x):
        if self.initialized:
            y = self.sos[0] * x + self.w[0]
            self.w[0] = self.sos[1] * x - self.sos[4] * y + self.w[1]
            self.w[1] = self.sos[2] * x - self.sos[5] * y
        else:
            y = x * sum(self.sos[:3]) / sum(self.sos[3:])
            self.w[1] = self.sos[2] * x - self.sos[5] * y
            self.w[0] = self.sos[1] * x - self.sos[4] * y + self.w[1]
            self.initialized = True
        return y


class IIR(Smoother):

    def __init__(self, alpha, beta=0, window_len=0):
        super().__init__(alpha, 0, 0)
        self.Wn = 0.5 * alpha
        self.sos = signal.butter(N=2, Wn=self.Wn, btype='lowpass', output='sos')
        self.biquads = [Biquad(s) for s in self.sos]
        self.initialized = True
        self.prev_y = None

    def reset(self):
        super().reset()

    def process(self, x):
        y = x
        for biquad in self.biquads:
            y = biquad.process(y)
        diff_y = y - self.prev_y if self.prev_y is not None else 0
        self.prev_y = y
        return y, diff_y
