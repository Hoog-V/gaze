import sys
from os import sep
from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtWidgets import QApplication
from mainWindow import MainWindow
from log import LogWindow
from videostreamer import VideoStreamer
from landmarkDetector import LandmarkDetector
from drawers import LandmarkDrawer
from headRotationEstimator import HeadRotationEstimator
from gazeDirectionEstimator import GazeDirectionEstimator
from dataWriter import DataWriter
from TCPServer import TCPServer

CFG_PATH = "cfg"

if __name__ == '__main__':
    print(f"Python version: {sys.executable}")

    settings_filename = sep.join([CFG_PATH, 'settings.ini'])
    settings = QSettings(settings_filename, QSettings.IniFormat)

    app = QApplication(sys.argv)
    mw = MainWindow(settings)
    lw = LogWindow(settings)
    vs = VideoStreamer(settings)
    lm = LandmarkDetector(settings)
    hre = HeadRotationEstimator(settings)
    gde = GazeDirectionEstimator(settings)
    lmd = LandmarkDrawer(settings)
    dwr = DataWriter(settings)
    tss = TCPServer(settings)

    # Connect logging signals
    vs.postMessage.connect(lw.append)
    lm.postMessage.connect(lw.append)
    hre.postMessage.connect(lw.append)
    gde.postMessage.connect(lw.append)
    lmd.postMessage.connect(lw.append)
    dwr.postMessage.connect(lw.append)
    tss.postMessage.connect(lw.append)

    # Connect processing signals
    vs.frame.connect(lm.update) #, type=Qt.BlockingQueuedConnection)
    lm.result.connect(hre.update)
    hre.result.connect(gde.update)
    gde.result.connect(dwr.update) #, type=Qt.QueuedConnection)
    gde.result.connect(tss.update) #, type=Qt.QueuedConnection)
    gde.result.connect(lmd.update)
    lmd.frame.connect(mw.update)
    # vs.frame.connect(mw.update)

    # connect buttons
    mw.anonymized_check_box.stateChanged.connect(lmd.anonymize)
    mw.record_button.toggled.connect(dwr.toggle)

    # Connect closing signals
    mw.closed.connect(lw.close, type=Qt.QueuedConnection)
    lw.closed.connect(mw.close, type=Qt.QueuedConnection)
    lw.closed.connect(vs.stop, type=Qt.QueuedConnection)
    lw.closed.connect(lm.stop, type=Qt.QueuedConnection)
    lw.closed.connect(hre.stop, type=Qt.QueuedConnection)
    lw.closed.connect(gde.stop, type=Qt.QueuedConnection)
    lw.closed.connect(lmd.stop, type=Qt.QueuedConnection)
    lw.closed.connect(dwr.stop, type=Qt.QueuedConnection)
    lw.closed.connect(tss.stop, type=Qt.QueuedConnection)

    # Start the show
    mw.move(100, 50)
    mw.resize(1500, 500)
    lw.move(100, 750)
    lw.resize(1500, 200)
    lw.show()
    mw.show()

    # Start-up recipe

    app.exec_()
