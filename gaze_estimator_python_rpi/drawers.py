import cv2
from PyQt5.QtCore import QSettings, QObject, pyqtSlot, pyqtSignal
import numpy as np
import mediapipe as mp
from mediapipe.framework.formats import landmark_pb2
from mediapipe.tasks.python.vision import FaceLandmarkerResult
from mediapipe import solutions
from helpers import convert_to_image_space
from eulerangles import euler2matrix


def draw_landmarks_on_image(mp_image, detection_result):
    face_landmarks_list = detection_result.face_landmarks
    annotated_mp_image = np.copy(mp_image)

    # Loop through the detected faces to visualize.
    for idx in range(len(face_landmarks_list)):
        face_landmarks = face_landmarks_list[idx]

        # Draw the face landmarks.
        face_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
        face_landmarks_proto.landmark.extend([
            landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in face_landmarks
        ])

        solutions.drawing_utils.draw_landmarks(
            image=annotated_mp_image,
            landmark_list=face_landmarks_proto,
            connections=mp.solutions.face_mesh.FACEMESH_TESSELATION,
            landmark_drawing_spec=None,
            connection_drawing_spec=mp.solutions.drawing_styles
                .get_default_face_mesh_tesselation_style())
        solutions.drawing_utils.draw_landmarks(
            image=annotated_mp_image,
            landmark_list=face_landmarks_proto,
            connections=mp.solutions.face_mesh.FACEMESH_CONTOURS,
            landmark_drawing_spec=None,
            connection_drawing_spec=mp.solutions.drawing_styles
                .get_default_face_mesh_contours_style())
        solutions.drawing_utils.draw_landmarks(
            image=annotated_mp_image,
            landmark_list=face_landmarks_proto,
            connections=mp.solutions.face_mesh.FACEMESH_IRISES,
            landmark_drawing_spec=None,
            connection_drawing_spec=mp.solutions.drawing_styles
                .get_default_face_mesh_iris_connections_style())

    return annotated_mp_image


def draw_vector_on_image(mp_image, rotation, translation, line_width):
    if mp_image.shape[2] != 3:
        raise ValueError('Input image must contain three channel bgr data.')

    image_rows, image_cols, _ = mp_image.shape

    # Create axis points in camera coordinate frame.
    axis_length = .1
    axis_world = np.float32([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]])
    axis_cam = np.matmul(rotation, axis_length * axis_world.T).T + translation
    x = axis_cam[..., 0]
    y = axis_cam[..., 1]
    z = axis_cam[..., 2]

    # Convert to image space.
    x_im, y_im = convert_to_image_space(mp_image.shape, (x, y))
    # x_im = np.int32(x * image_cols)
    # y_im = np.int32(y * image_rows)

    # Draw xyz axis on the image.
    origin = (x_im[0], y_im[0])
    x_axis = (x_im[1], y_im[1])
    y_axis = (x_im[2], y_im[2])
    z_axis = (x_im[3], y_im[3])

    annotated_mp_image = np.copy(mp_image)
    cv2.arrowedLine(annotated_mp_image, origin, x_axis, (0, 0, 255), line_width)
    cv2.arrowedLine(annotated_mp_image, origin, y_axis, (0, 255, 0), line_width)
    cv2.arrowedLine(annotated_mp_image, origin, z_axis, (255, 0, 0), line_width)

    return annotated_mp_image


class LandmarkDrawer(QObject):
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    frame = pyqtSignal(np.ndarray)

    def __init__(self, settings: QSettings = None):
        super().__init__()
        self.anonymized = False
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        self.face_nr = 0  # if there are multiple, pick first
        print(f"{self.__class__.__name__}: info; initialized")

    @pyqtSlot(np.ndarray)
    def update(self, image: np.ndarray = None):
        pass

    # For some reason method overloading gives errors, so need to comment this out:
    # @pyqtSlot(np.ndarray, FaceLandmarkerResult)
    # def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None):
    #     if image is None:
    #         return
    #     if detection_results is None or len(detection_results.face_landmarks) == 0:
    #         return
    #     mp_image = mp.Image(image_format=mp.ImageFormat.SRGB,
    #                         data=np.zeros_like(image) if self.anonymized else image)
    #     image = draw_landmarks_on_image(mp_image.numpy_view(), detection_results)
    #     self.frame.emit(image)

    # @pyqtSlot(np.ndarray, FaceLandmarkerResult, list)
    # def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None,
    #            head_rotation: list = None):
    #     if image is None:
    #         return
    #     if detection_results is None or len(detection_results.face_landmarks) == 0:
    #         return
    #     if head_rotation is None or len(head_rotation) == 0:
    #         return
    #     mp_image = mp.Image(image_format=mp.ImageFormat.SRGB,
    #                         data=np.zeros_like(image) if self.anonymized else image)
    #     image = draw_landmarks_on_image(mp_image.numpy_view(), detection_results)
    #
    #     face_landmarks = detection_results.face_landmarks[self.face_nr]
    #     origin = face_landmarks[168]  # midwayBetweenEyes
    #     translation = np.array([origin.x, origin.y, origin.z])
    #     rotation = detection_results.facial_transformation_matrixes[self.face_nr][:3, :3]
    #     image = draw_head_pose_vector_on_image(image, rotation, translation)
    #     self.frame.emit(image)

    @pyqtSlot(np.ndarray, FaceLandmarkerResult, list, list)
    def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None,
               head_rotation: list = None, gaze_direction: list = None):
        if image is None:
            return
        if detection_results is None or len(detection_results.face_landmarks) == 0:
            return
        if head_rotation is None or len(head_rotation) == 0:
            return
        if gaze_direction is None or len(gaze_direction) == 0:
            return
        mp_image = mp.Image(image_format=mp.ImageFormat.SRGB,
                            data=np.zeros_like(image) if self.anonymized else image)
        image = draw_landmarks_on_image(mp_image.numpy_view(), detection_results)

        face_landmarks = detection_results.face_landmarks[self.face_nr]
        # origin = face_landmarks[168]  # midwayBetweenEyes
        # translation = np.array([origin.x, origin.y, origin.z])
        # rotation = euler2matrix(head_rotation, axes='yxz', intrinsic=False, right_handed_rotation=True)
        # image = draw_vector_on_image(image, rotation, translation, 1)

        origin = face_landmarks[468]  # left_iris_centre
        translation = np.array([origin.x, origin.y, origin.z])
        rotation = euler2matrix(gaze_direction, axes='yxz', intrinsic=False, right_handed_rotation=False)
        image = draw_vector_on_image(image, rotation, translation, 3)

        origin = face_landmarks[473]  # right_iris_centre
        translation = np.array([origin.x, origin.y, origin.z])
        rotation = euler2matrix(gaze_direction, axes='yxz', intrinsic=False, right_handed_rotation=False)
        image = draw_vector_on_image(image, rotation, translation, 3)

        self.frame.emit(image)

    @pyqtSlot(int)
    def anonymize(self, val: int):
        """ Anonymize frame
            :param val: boolean
        """
        self.postMessage.emit(f"{self.__class__.__name__}: info; {'' if val else 'no'} anonymization")
        self.anonymized = val

    @pyqtSlot()
    def stop(self):
        try:
            pass
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

