"""@package docstring
Logging object
"""
from os import sep
from time import localtime, strftime
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QTextEdit
from PyQt5.QtGui import QCloseEvent


class LogWindow(QWidget):
    closed = pyqtSignal()

    def __init__(self, settings):
        super().__init__()
        self.log_file_name = None

        if settings is not None:
            print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
            self.settings = settings
            # storage_path = settings.value('temp_folder', None, type=str)
            # self.log_file_name = sep.join([storage_path, 'temp.log'])  # why here???

        self.setWindowTitle("Log")
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.log = QTextEdit()
        layout.addWidget(self.log)
        print(f"{self.__class__.__name__}: info; initialized, logging to {self.log_file_name}")

    @pyqtSlot(str)
    def append(self, s):
        self.log.append(s)
        if self.log_file_name is not None:
            with open(self.log_file_name, 'a+') as log_file:
                s = s if '\n' in s else s + '\n'
                s = strftime("%y%m%d_%H:%M:%S", localtime()) + ";" + s
                log_file.write(s)

    def closeEvent(self, event: QCloseEvent):
        self.closed.emit()
        print(f"{self.__class__.__name__}: info; stopped")
        event.accept()
