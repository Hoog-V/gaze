from PyQt5.QtCore import QSettings, QObject, pyqtSlot, pyqtSignal
from mediapipe.tasks.python.vision import FaceLandmarkerResult
import numpy as np
from eulerangles import matrix2euler
from smoothers import IIR


class HeadRotationEstimator(QObject):
    finished = pyqtSignal()
    postMessage = pyqtSignal(str)
    result = pyqtSignal(np.ndarray, FaceLandmarkerResult, list)

    def __init__(self, settings: QSettings = None):
        super().__init__()
        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings
        self.decimals = self.settings.value('decimals', 1, int)
        self.face_nr = 0  # if there are multiple, pick first
        self.alpha = self.settings.value('alpha', 1, float)
        self.alpha = self.settings.value('HeadRotationEstimator/alpha', 1, float)
        self.angleSmoothers = [IIR(self.alpha), IIR(self.alpha), IIR(self.alpha)]
        print(f"{self.__class__.__name__}: info; initialized.")

    @pyqtSlot(np.ndarray, FaceLandmarkerResult)
    def update(self, image: np.ndarray = None, detection_results: FaceLandmarkerResult = None):
        if image is None:
            return
        if detection_results is None or len(detection_results.face_landmarks) == 0:
            return

        rotation = detection_results.facial_transformation_matrixes[self.face_nr][:3, :3]
        yaw, pitch, roll = matrix2euler(rotation, axes='yxz', intrinsic=False, right_handed_rotation=True)
        yaw, _ = self.angleSmoothers[0].process(yaw)
        pitch, _ = self.angleSmoothers[1].process(pitch)
        roll, _ = self.angleSmoothers[2].process(roll)
        yaw = round(yaw, self.decimals)
        pitch = round(pitch, self.decimals)
        roll = round(roll, self.decimals)
        # self.postMessage.emit(f"{self.__class__.__name__}: info; "
        #                       f"head orientation: yaw={yaw:2.1f}°, pitch={pitch:2.1f}°, roll={roll:2.1f}°")

        self.result.emit(image, detection_results, [yaw, pitch, roll])

    @pyqtSlot()
    def stop(self):
        try:
            pass
        except Exception as err:
            self.postMessage.emit(f"{self.__class__.__name__}: error; type: {type(err)}, args: {err.args}")

