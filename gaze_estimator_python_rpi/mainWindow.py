"""@package docstring
MainWindow
"""
import cv2
from time import time
import numpy as np
import matplotlib
from PyQt5.QtGui import QCloseEvent, QImage, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QTimer, QEventLoop, QSettings
from PyQt5.QtWidgets import QMainWindow, QWidget, QDesktopWidget, QLabel, QPushButton, QProgressBar, QSpinBox, \
    QDoubleSpinBox, QGridLayout, QHBoxLayout, QSpacerItem, QSizePolicy, QCheckBox

# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt


class MainWindow(QMainWindow):
    postMessage = pyqtSignal(str)
    closed = pyqtSignal()

    settings: QSettings
    image: np.array

    def __init__(self, settings=None):
        """

        :type settings: object
        """
        super(MainWindow, self).__init__()

        print(f"{self.__class__.__name__}: info; loading settings from {settings.fileName()}")
        self.settings = settings

        frame_size_str = self.settings.value('gui_frame_size')
        (width, height) = frame_size_str.split('x')
        self.image_size = (int(width), int(height))

        self.central_widget = QWidget()  # define central widget
        self.setCentralWidget(self.central_widget)  # set QMainWindow.centralWidget

        # self.image = None
        self.prev_clock_time = None

        self.setWindowTitle(self.settings.value('name'))
        screen = QDesktopWidget().availableGeometry()
        self.image_width = round(screen.height() * 0.8)
        self.image_height = round(screen.width() * 0.8)
        self.image_scaling_factor = 1.0
        self.image_scaling_step = 0.1

        # Labels
        self.PixImage = QLabel()
        self.anonymized_check_box_label = QLabel("anonymize")

        # Inputs
        self.record_button = QPushButton("record")
        self.record_button.setCheckable(True)
        self.anonymized_check_box = QCheckBox(self)

        # a figure instance to plot on
        self.canvas = FigureCanvas(Figure())  # (figsize=(5, 3)))
        # self.axes = self.canvas.figure.subplots(2, 2, sharex=False, sharey=False)

        # Compose layout grid
        widget_layout = QGridLayout()
        widget_layout.addWidget(self.anonymized_check_box_label, 0, 0, Qt.AlignRight)
        widget_layout.addWidget(self.anonymized_check_box, 0, 1, Qt.AlignLeft)
        widget_layout.addWidget(self.record_button, 1, 0, alignment=Qt.AlignLeft)
        # # Compose final layout
        # hlayout = QHBoxLayout()
        # hlayout.setSpacing(10)
        # hlayout.addLayout(widgetLayout)

        layout = QHBoxLayout()
        layout.addLayout(widget_layout, Qt.AlignTop | Qt.AlignCenter)
        layout.addWidget(self.PixImage, Qt.AlignTop | Qt.AlignCenter)
        layout.addWidget(self.canvas, Qt.AlignTop | Qt.AlignCenter)
        self.centralWidget().setLayout(layout)
        print(f"{self.__class__.__name__}: info; initialized")

    def closeEvent(self, event: QCloseEvent):
        self.closed.emit()
        print(f"{self.__class__.__name__}: info; stopped")
        event.accept()

    def wheelEvent(self, event):
        if (event.angleDelta().y() > 0) and (self.image_scaling_factor > self.image_scaling_step):  # zooming in
            self.image_scaling_factor -= self.image_scaling_step
        elif (event.angleDelta().y() < 0) and (self.image_scaling_factor < 1.0):  # zooming out
            self.image_scaling_factor += self.image_scaling_step
        self.image_scaling_factor = round(self.image_scaling_factor, 2)  # strange behaviour, so rounding is necessary
        self.update()  # redraw the image with different scaling

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    @pyqtSlot(np.ndarray)
    def update(self, image=None):
        if image is not None:  # we have a new image
            height, width = image.shape[:2]  # get dimensions
            self.image = image if self.image_size[0] == width and self.image_size[1] == height \
                else cv2.resize(image, self.image_size)
            if 0 < self.image_scaling_factor < 1:  # Crop the image to create a zooming effect
                height, width = image.shape[:2]  # get dimensions
                delta_height = round(height * (1 - self.image_scaling_factor) / 2)
                delta_width = round(width * (1 - self.image_scaling_factor) / 2)
                image = image[delta_height:height - delta_height, delta_width:width - delta_width]
            height, width = image.shape[:2]  # get dimensions
            if self.image_height != height or self.image_width != width:  # we need scaling
                scaling_factor = self.image_height / float(height)  # get scaling factor
                if self.image_width / float(width) < scaling_factor:
                    scaling_factor = self.image_width / float(width)
                    image = cv2.resize(image, None, fx=scaling_factor, fy=scaling_factor,
                                       interpolation=cv2.INTER_AREA)  # resize image
            if len(image.shape) < 3:  # check nr of channels
                image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)  # convert to color image
            height, width = image.shape[:2]  # get dimensions
            q_image = QImage(image.data, width, height, width * 3,
                             QImage.Format_RGB888)  # Convert from OpenCV to PixMap
            self.PixImage.setPixmap(QPixmap(q_image))
            self.PixImage.show()

    @pyqtSlot(int)
    def updateProgressBar(self, val):
        if 0 <= val <= 100:
            self.progress_bar.setValue(val)
