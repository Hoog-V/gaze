# Cross compiling for the RPI

## Prerequisites PC
You will need to have the following software installed on your pc:
<details>
<summary>Docker</summary>

**Windows** Install the .exe installer from [Docker's official website](https://www.docker.com/products/docker-desktop/)

**Linux- Ubuntu:** Run this command: `sudo apt-get install docker.io docker-buildx`

**Linux- Arch:** Run this command: `sudo pacman -S docker docker-buildx` 

</details>

<details>
<summary> Preferably any IDE with docker support (CLion is used in this guide) </summary>

**Windows** Install the .exe installer from [Jetbrains website](https://www.jetbrains.com/clion/)

**Linux- Any** Download and follow the instructions given in the tar.gz to install clion [Jetbrains website](https://www.jetbrains.com/clion/)

Alternatively when using arch you can also download the package from aur using yay -> `sudo pacman -S yay` and `yay -S clion`
</details>

<details>
<summary> A fresh clone of this repository </summary>

Open a CMD/terminal and run: `git clone https://gitlab.com/Hoog-V/gaze.git --recursive`
</details>

## Prerequisites Raspberry Pi:

You need to have an install of Bookworm with Desktop Environment (**ANOTHER VERSION OF DEBIAN WON'T WORK**).

The following packages have to be installed:

- gdb
- libcamera-dev
- libopencv-dev 

`sudo apt-get install gdb libcamera-dev libopencv-dev`

Besides the packages you will also need:

- SSH enabled and setup:
[Guide](https://phoenixnap.com/kb/enable-ssh-raspberry-pi)

Run an update to make sure that package versions don't differ to much between our docker and Raspberry Pi image:

`sudo apt-get update && sudo apt-get upgrade` 


### Let's get started

1. **Go to the `GAZE_ESTIMATOR_CPP` folder**
<details><summary>Open a terminal/cmd line here</summary>

**Windows**: Navigate to the folder `GAZE_ESTIMATOR_CPP`, **Right click** then select: **Open in terminal** or **Open PowerShell window here**

**Linux and Mac**: You will probably already know how to do this :)
</details>

2. **Run the following command:**
    
    `docker run  --restart unless-stopped --privileged multiarch/qemu-user-static --reset -p yes`

    This command will enable qemu instruction translation for our aarch64 image. 
    
    Without running this command the x86_64 architecture based computer you're probably running will not understand the arm64 
    instructions needed to run the Raspberry Pi image.

    The restart argument in this command makes sure that this image is ran everytime you restart your computer.

3. **Then build the container:**

    `docker buildx build -f Dockerfile --tag cross-compile-img .`

    This command will build the container using the Dockerfile (which contains the packages and stuff needed to setup this special Raspberry Pi image). It will then name this image cross-compile-img.

4. **Open the project in CLion**

    **If there is a project already opened: File->Close project**

    Open CLion and on the `Welcome to CLion` screen: 
    
    1. **Select the option `Open`**
    
    2. Then navigate and open the `GAZE_ESTIMATOR_CPP` folder

    3. Then CLion will ask you to setup your CMake toolchain, select the following option:
       - Toolchain: -> Manage Toolchains
    4. This will open a new settings page called `Toolchains`. 
        
        Do the following:

        - Click the + sign in the bar on the top left of the settings screen.

        - Then select the option Docker

        - Make sure that the `Image:` option is set to cross-compile-img:latest

        - Leave everything at default and click on `OK`
    5. Now you are back at the previous screen: `CMake`
        
        Fill in the following final options:

        Build type: `RelWithDebInfo`

        Toolchain: `Docker`

        Generator: `Ninja`
    6. Click on OK and wait for CMake to finish generating the Build files.

5. When CMake is done generating open the `Edit run configurations screen`





