# Native building

## Prerequisites
<details>
<summary> C++ toolchain</summary>

**Windows:** Use MSVC from the Visual Studio 2019 build tools package.
			 Also download and install Ninja as well (for faster builds).
			 **See section Windows for further instructions!**
			 
In general GCC has better compatibility with most IDE's, though Clang should work too:

**Ubuntu:** `sudo apt-get install build-essential ninja-build gdb`

**Arch:** `sudo pacman -S gcc ninja-build gdb`

</details>

<details>
<summary> CMake </summary>

Ah yes here is our big friend CMake :)

In Linux you can just use the package managers:

**Ubuntu:** `sudo apt-get install cmake`

**Arch:** `sudo pacman -S cmake`

Installation on Windows can be done multiple ways. 
The easiest method is by installing it using the VS build tools installer.

</details>

<details>
<summary>OpenCV</summary>

**Windows:** Download the latest precompiled binaries from opencv. Extract them to a good location which isn't affected easily by other programs or user actions. **See section Windows for further instructions!**

**Ubuntu:** `sudo apt-get install libopencv-dev`

**Arch:** `sudo pacman -S opencv`

</details>

<details>
<summary>Boost</summary>

**Windows:** Download the latest precompiled binaries from [boost](https://www.boost.org/). Extract them to a good location which isn't affected easily by other programs or user actions. **See section Windows for further instructions!**

**Ubuntu:** tbd

**Arch:** tbd

</details>

<details>
<summary> A good IDE with debugging capability </summary>

I Recommend CLion or VS code.
</details>



### Windows

Set-up on Windows is pretty involved. That's why a complete section is dedicated to this. 

#### VS build tools 2019

The OpenCV package in the most recent releases only has precompiled binaries compatible with the MSVC 2019 compiler. 

That's why this specific package is needed. [Download it from here](https://my.visualstudio.com/Downloads?q=visual%20studio%202019&wt.mc_id=o~msft~vscom~older-downloads). 

**Note downloading requires you to login to your Microsoft account, perks that come with using Windows I guess**

Then on the installation screen select these installation options:

![](VS_buildtools.png)


**Instead of 2022 select the 2019 options :)**


#### CMake

You probably selected the C++ CMake tools for Windows option. Then you are ready to go!

#### Ninja

[//]: # (First install [scoop]&#40;https://scoop.sh/&#41;)

[//]: # (After installing scoop run this in Powershell:)

[//]: # (```ps)

[//]: # (scoop install ninja)

[//]: # (```)
run this in Powershell:

```ps
pip install ninja
```

#### OpenCV

OpenCV can be installed by downloading the [latest release](https://sourceforge.net/projects/opencvlibrary/files/)
of the precompiled binaries.

Then run the installer, please select a good installpath. I recommend putting it in C:/OpenCV or something similar.

##### Add to PATH

And now we come to the most annoying step. Adding the files to path.

Open the System Variables tab:

`WIN` + `R` -> SYSDM.CPL -> Advanced -> System Variables

Then add the `OPENCV_DIR` variable to path. 

**This variable should be set to the build folder inside the opencv folder you just installed.**
For example if installed in C:\OpenCV the variable has to be set to C:\OpenCV\build!

Then add to the `PATH` variable the two paths: `%OPENCV_DIR%/bin` and `%OPENCV_DIR%/x64/vc16/bin`

Close and you're now done with installing opencv.

#### Boost

Boost can be installed by downloading the [latest release](https://www.boost.org/doc/libs/1_84_0/more/getting_started/windows.html)
of the precompiled binaries.

For this project, there is no need to install Boost, as downloaded Boost provides header-only libraries for the stuff we need.


#### The IDE

You can now open the IDE of your liking with CMake support. 
Select in the toolchain options the option for Visual Studio toolchain. 
It should automatically pick-up your MSVC 2019 compiler.

#### Configure Boost
Go to File -> Settings -> Build, Execution, Deployment -> CMake, and in CMake options add:

 -DBOOST_ROOT="C:\Program Files\boost\boost_1_82_0"

 -DBOOST_INCLUDE_DIR="C:\Program Files\boost\boost_1_82_0\boost"

 -DBOOST_LIBRARY_DIR="C:\Program Files\boost\boost_1_82_0\libs"


