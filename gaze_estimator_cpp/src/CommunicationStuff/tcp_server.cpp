#include "tcp_server.h"
#include <boost/asio.hpp>


#include <iostream>

using boost::asio::ip::tcp;
using namespace std::chrono_literals;

void send_(tcp::socket & socket, const std::string& message) {
    const std::string msg = message + "\n";
    boost::asio::write( socket, boost::asio::buffer(message) );
}


class Session : public std::enable_shared_from_this<Session> {
public:
    Session(tcp::socket socket) : socket_(std::move(socket)) {}

    void start() {

        doWrite();
    }

private:
//    const char *jsonFormatString = "{\"head_rotation\": [%f, %f, %f], \"gaze_direction\": [%f, %f, %f]}";
    const char *jsonFormatString = "{\"face detected\": %i, \"head position\": [%f, %f, %f]}";

   char buffer[512];
    void doWrite() {
        boost::interprocess::message_queue mq(boost::interprocess::open_only, "message_queue");
//        calculated_angles receivedMessage;
        Results receivedMessage;
        size_t receivedSize;
        unsigned int priority;
        try{
          mq.receive(&receivedMessage, sizeof(receivedMessage), receivedSize, priority);
        } catch (boost::interprocess::interprocess_exception &ex) {
            std::cerr << "Interprocess Exception: " << ex.what() << std::endl;
        }
        memset(buffer, '\0', sizeof(buffer));
//        snprintf(buffer, sizeof(buffer), jsonFormatString, receivedMessage.head_angle[0],
//                                                           receivedMessage.head_angle[1],
//                                                           receivedMessage.head_angle[2],
//                                                           receivedMessage.gaze_angle[0],
//                                                           receivedMessage.gaze_angle[1],
//                                                           receivedMessage.gaze_angle[2]);
//
        snprintf(buffer, sizeof(buffer), jsonFormatString, receivedMessage.faceDetectedFlag,
                 receivedMessage.headTranslation[0],
                 receivedMessage.headTranslation[1],
                 receivedMessage.headTranslation[2]);

        auto self(shared_from_this());
        boost::asio::async_write(socket_, boost::asio::buffer(buffer, sizeof(buffer)),
            [this, self](boost::system::error_code ec, std::size_t /*length*/) {
                if (!ec) {
                    // Continue sending the message as long as there's no error
                    doWrite();
                } else {
                    // Handle error or disconnection
                    std::cerr << "Error on sending: " << ec.message() << std::endl;
                }
            });
    }

    tcp::socket socket_;
};

class Server {
public:
    Server(boost::asio::io_context& io_context, short port)
        : acceptor_(io_context, tcp::endpoint(tcp::v4(), port)) {
        doAccept();
    }

private:
    void doAccept() {
        acceptor_.async_accept([this](boost::system::error_code ec, tcp::socket socket) {
            if (!ec) {
                std::make_shared<Session>(std::move(socket))->start();
            }

            doAccept();
        });
    }

    tcp::acceptor acceptor_;
};



void tcp_server_thread() {
while(1) {
    try {
        boost::asio::io_context io_context;
        Server server(io_context, 5678); // Port number

        io_context.run();
    } catch (std::exception &e) {
        std::cerr << "Exception: " << e.what() << "\n";
    }
}
}