#ifndef GAZE_TRACKER_TCP_SERVER_H
#define GAZE_TRACKER_TCP_SERVER_H
#include <boost/interprocess/ipc/message_queue.hpp>
#include <opencv2/core.hpp>
#include <mutex>

typedef struct calculated_angles {
    cv::Vec3f head_angle;
    cv::Vec3f gaze_angle;
};

typedef struct Results {
    bool faceDetectedFlag;
    cv::Vec3f headTranslation;
};

void tcp_server_thread();

#endif //GAZE_TRACKER_TCP_SERVER_H
