#ifndef GAZE_TRACKER_MESHANNOTATIONS_H
#define GAZE_TRACKER_MESHANNOTATIONS_H

#include <iostream>
#include <vector>
#include <unordered_map>

/**
 * @brief An enum representing mesh annotations for facial landmarks.
 * This maps directly to the array 'face_mesh_annotations'
 *
 * This map associates string keys with vectors of integers representing landmark indices.
 * The indices correspond to specific facial landmarks on the mesh.
 * From: https://github.com/tensorflow/tfjs-models/blob/838611c02f51159afdd77469ce67f0e26b7bbb23/face-landmarks-detection/src/mediapipe-facemesh/keypoints.ts
 */
enum face_mesh {
    silhouette,
    lipsUpperOuter,
    lipsLowerOuter,
    lipsUpperInner,
    lipsLowerInner,
    rightEyeUpper0,
    rightEyeLower0,
    rightEyeUpper1,
    rightEyeLower1,
    rightEyeUpper2,
    rightEyeLower2,
    rightEyeLower3,
    rightEyebrowUpper,
    rightEyebrowLower,
    rightEyeIris,
    leftEyeUpper0,
    leftEyeLower0,
    leftEyeUpper1,
    leftEyeLower1,
    leftEyeUpper2,
    leftEyeLower2,
    leftEyeLower3,
    leftEyebrowUpper,
    leftEyebrowLower,
    leftEyeIris,
    midwayBetweenEyes,
    noseTip,
    noseBottom,
    noseRightCorner,
    noseLeftCorner,
    rightCheek,
    leftCheek
};


const uint16_t face_mesh_annotations[][36] = {{10,  338, 297, 332, 284, 251, 389, 356, 454, 323, 361, 288, 397, 365, 379, 378, 400, 377, 152, 148, 176, 149, 150, 136, 172, 58, 132, 93, 234, 127, 162, 21, 54, 103, 67, 109},
                                        {61,  185, 40,  39,  37,  0,   267, 269, 270, 409, 291},
                                        {146, 91,  181, 84,  17,  314, 405, 321, 375, 291},
                                        {78,  191, 80,  81,  82,  13,  312, 311, 310, 415, 308},
                                        {78,  95,  88,  178, 87,  14,  317, 402, 318, 324, 308},
                                        {246, 161, 160, 159, 158, 157, 173},
                                        {33,  7,   163, 144, 145, 153, 154, 155, 133},
                                        {247, 30,  29,  27,  28,  56,  190},
                                        {130, 25,  110, 24,  23,  22,  26,  112, 243},
                                        {113, 225, 224, 223, 222, 221, 189},
                                        {226, 31,  228, 229, 230, 231, 232, 233, 244},
                                        {143, 111, 117, 118, 119, 120, 121, 128, 245},
                                        {156, 70,  63,  105, 66,  107, 55,  193},
                                        {35,  124, 46,  53,  52,  65},
                                        {473, 474, 475, 476, 477},
                                        {466, 388, 387, 386, 385, 384, 398},
                                        {263, 249, 390, 373, 374, 380, 381, 382, 362},
                                        {467, 260, 259, 257, 258, 286, 414},
                                        {359, 255, 339, 254, 253, 252, 256, 341, 463},
                                        {342, 445, 444, 443, 442, 441, 413},
                                        {446, 261, 448, 449, 450, 451, 452, 453, 464},
                                        {372, 340, 346, 347, 348, 349, 350, 357, 465},
                                        {383, 300, 293, 334, 296, 336, 285, 417},
                                        {265, 353, 276, 283, 282, 295},
                                        {468, 469, 470, 471, 472},
                                        {168},
                                        {1},
                                        {2},
                                        {98},
                                        {327},
                                        {205},
                                        {425}};
#endif //GAZE_TRACKER_MESHANNOTATIONS_H
