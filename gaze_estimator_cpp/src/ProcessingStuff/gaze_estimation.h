#ifndef GAZE_TRACKER_GAZE_ESTIMATION_H
#define GAZE_TRACKER_GAZE_ESTIMATION_H
#include <opencv2/core.hpp>

cv::Vec3f gaze_direction_estimation(std::vector<cv::Vec3f> &face_landmarks);
float calculateCameraDistance(std::vector<cv::Vec3f> &face_landmarks, uint32_t width);


#endif //GAZE_TRACKER_GAZE_ESTIMATION_H
