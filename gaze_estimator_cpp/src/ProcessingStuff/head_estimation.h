#ifndef GAZE_TRACKER_HEAD_ESTIMATION_H
#define GAZE_TRACKER_HEAD_ESTIMATION_H

#include <opencv2/core.hpp>
cv::Vec3f head_angle_estimation(std::vector<cv::Vec3f> face_landmarks);

#endif //GAZE_TRACKER_HEAD_ESTIMATION_H
