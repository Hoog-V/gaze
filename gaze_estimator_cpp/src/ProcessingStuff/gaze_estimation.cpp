#include "MeshAnnotations.h"
#include "gaze_estimation.h"

float clampAngle(float angle) {
    while (angle > CV_PI / 2) {
        angle -= CV_PI;
    }
    while (angle < -CV_PI / 2) {
        angle += CV_PI;
    }
    return angle;
}

float calculateCameraDistance(std::vector<cv::Vec3f> &face_landmarks, uint32_t width) {
    // average size of human iris is 11.7mm - fairly constant for all ages/genders/races

    // Iris points are [center, left, top, right, bottom]
    const int leftIrisLeftCoordinate = face_mesh_annotations[leftEyeIris][1];
    const int leftIrisRightCoordinate = face_mesh_annotations[leftEyeIris][3];
    const int rightIrisLeftCoordinate = face_mesh_annotations[rightEyeIris][1];
    const int rightIrisRightCoordinate = face_mesh_annotations[rightEyeIris][3];

    // Calculate iris size in pixels (normalized to 0..1)
    const float leftIrisSize = std::abs(face_landmarks[leftIrisLeftCoordinate][0] -
            face_landmarks[leftIrisRightCoordinate][0]) / (float) width;
    const float rightIrisSize = std::abs(face_landmarks[rightIrisLeftCoordinate][0] -
            face_landmarks[rightIrisRightCoordinate][0]) / (float) width;
    const float irisSize = std::max(leftIrisSize, rightIrisSize);

    // Calculate camera distance in meters (rounded to two decimal places)
    const float cameraDistance = std::round(0.5 * 117.0f / irisSize) / 10000.0f;

//# Simplified distance estimation based only on y-coordinate
//    iris_radius = average_iris_size / 2
//    distance = iris_radius / abs(iris_center[1])
    return cameraDistance;
}

cv::Vec3f gaze_direction_estimation(std::vector<cv::Vec3f> &face_landmarks) {
    /*
     * Gaze direction estimation is based on the following approach:
     *
     *  1. Pick the eye that is most visible, e.g. closest to the camera, or the one with the largest palpebral
     *  fissure width. The latter seems to be more robust.
     *  2. Define the extreme coordinates of the eye, i.e. top, bottom, left, right.
     *  Left and right are defined on the lateral commissure and lacrimal caruncle, respectively.
     *  Top and bottom points are not defined on eyelids but on the eye socket, so they are not affected by blinking.
     *  3. Define the center of the eye as the average of the left and right extreme coordinates.
     *  4. Compute gaze direction as the angle between the eye center and the iris center.
     *  Now, given the radius of the eyeball and the center of the eye, and iris center, we can estimate the pitch and
     *  yaw of the eye, e.g. float yaw = atan2(eye_diff.x, eye_size.x/2). However, the iris is not perfectly spherical,
     *  and it can be tilted or rotated slightly giving rise to an offset between the iris center and the eye center.
     *  A different approach is to assume there is an offset between the iris center and the eye center, roughly
     *  equivalent for adults 15 degrees in the vertical direction and 10 degrees in the horizontal direction.
     *  Next, we assume that eye pitch and yaw angles are relatively small, so we can approximate atan(x) by x, which is
     *  the ratio of the iris and eye center difference to the eye radius, mapped to the maximum range of angles we
     *  expect.
     *
     *  Dit werkt nog niet helemaal goed, afstellen op basis van de ooghoeken?
     */

    float max_delta_yaw = 35 * CV_PI / 180, max_delta_pitch = 70 * CV_PI / 180;
    float yaw_offset = 10 * CV_PI / 180, pitch_offset = 15 * CV_PI / 180; // offset to adjust for eye orientation

    // Estimate if left eye is closer than right
    bool left_is_close = (face_landmarks[33][0] - face_landmarks[133][0]) > (face_landmarks[362][0] - face_landmarks[263][0]);
//    bool left_is_close = face_landmarks[473][2] > face_landmarks[468][2];

    // Define extreme coordinates for left/right eye
    int topCoordinates = left_is_close ? 27 : 257;
    int bottomCoordinates = left_is_close ? 23 : 53;
    int leftCoordinates = left_is_close ? 133 : 263;
    int rightCoordinates = left_is_close ? 33 : 362;
    int centerCoordinates = left_is_close ? 473: 468;

    cv::Vec3f iris_center = face_landmarks[centerCoordinates];
    cv::Point2f eye_center = cv::Point2f((face_landmarks[leftCoordinates][0] + face_landmarks[rightCoordinates][0]) / 2,
                                         (face_landmarks[leftCoordinates][1] + face_landmarks[rightCoordinates][1]) / 2);
//    cv::Point2f eye_size = cv::Point2f(face_landmarks[leftCoordinates][0] - face_landmarks[rightCoordinates][0],
//                                       face_landmarks[topCoordinates][1] - face_landmarks[bottomCoordinates][1]);
    float eye_radius = abs(face_landmarks[leftCoordinates][0] - face_landmarks[rightCoordinates][0])/2;
    cv::Point2f eye_diff = cv::Point2f(eye_center.x - iris_center[0], (eye_center.y - iris_center[1]));
//    std::cout << "eye_diff: " << eye_diff << " eye_radius: " << eye_radius << std::endl;

    float pitch = max_delta_pitch * eye_diff.y / eye_radius - pitch_offset;
    float yaw = max_delta_yaw * eye_diff.x / eye_radius - yaw_offset;
//    float yaw = atan2(eye_diff.x, eye_size.x/2);
//    float pitch = atan2(eye_diff.y, eye_size.y/2);
    pitch = clampAngle(pitch);
    yaw = clampAngle(yaw);
//    std::cout << "yaw: " << yaw << " pitch: " << pitch << std::endl;

    return cv::Vec3f(pitch, yaw, 0);
}