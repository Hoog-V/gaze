//
// Created by victor on 16-1-24.
//
#include <condition_variable>
#include "head_estimation.h"


cv::Vec3f cross_vectors(const cv::Vec3f& a, const cv::Vec3f& b) {
    // Vector cross product (a x b)
    float x = a[1] * b[2] - a[2] * b[1];
    float y = a[2] * b[0] - a[0] * b[2];
    float z = a[0] * b[1] - a[1] * b[0];

    return cv::Vec3f(x, y, z);
}

cv::Vec3f rotation_matrix_to_euler_angle(const cv::Matx33f& r) {
    double thetaX, thetaY, thetaZ;

    if (r(1,0) < 1) {
        if (r(1,0) > -1) {
            thetaZ = std::asin(r(1,0));
            thetaY = std::atan2(-r(2,0), r(0,0));
            thetaX = std::atan2(-r(1,2), r(1,1));
        } else {
            thetaZ = -CV_PI / 2;
            thetaY = -std::atan2(r(2,1), r(2,2));
            thetaX = 0;
        }
    } else {
        thetaZ = CV_PI / 2;
        thetaY = std::atan2(r(2,1), r(2,2));
        thetaX = 0;
    }

    if (std::isnan(thetaX)) thetaX = 0;
    if (std::isnan(thetaY)) thetaY = 0;
    if (std::isnan(thetaZ)) thetaZ = 0;

    return cv::Vec3f(-thetaX, -thetaY, -thetaZ);
}


cv::Vec3f head_angle_estimation(std::vector<cv::Vec3f> face_landmarks) {
    std::vector<cv::Vec3f> bbox = {face_landmarks[10], // top
                                   face_landmarks[152], // bottom
                                   face_landmarks[234], // left
                                   face_landmarks[454]}; // right
    cv::Vec3f x_axis, y_axis, z_axis;
    cv::normalize(bbox[1] - bbox[0], y_axis);
    cv::normalize(bbox[3] - bbox[2], x_axis);
    cv::normalize(cross_vectors(x_axis, y_axis), z_axis);
    x_axis = cross_vectors(y_axis, z_axis);

    cv::Matx33f rotation_matrix(x_axis[0], y_axis[0], z_axis[0],
                               x_axis[1], y_axis[1], z_axis[1],
                               x_axis[2], y_axis[2], z_axis[2]);

    cv::Vec3f rotations = rotation_matrix_to_euler_angle(rotation_matrix);

    return rotations;
}

