#include "IrisLandmark.hpp"
#include <iostream>
#include <thread>

#define EYE_LANDMARKS 71
#define IRIS_LANDMARKS 5
#define FACE_LANDMARKS 468

/*
Helper functions
*/
bool __isEyeIndexValid(int idx) {
    if (idx < 0 || idx >= EYE_LANDMARKS) {
        std::cerr << "Index " << idx << " is out of range (" \
        << EYE_LANDMARKS << ")." << std::endl;
        return false;
    }
    return true;
}


bool __isIrisIndexValid(int idx) {
    if (idx < 0 || idx >= IRIS_LANDMARKS) {
        std::cerr << "Index " << idx << " is out of range (" \
        << IRIS_LANDMARKS << ")." << std::endl;
        return false;
    }
    return true;
}


Model::IrisLandmark::IrisLandmark(std::string modelPath):
    FaceLandmark(modelPath),
    m_leftIrisLandmarker(modelPath + std::string("/iris_landmark.tflite")),
    m_rightIrisLandmarker(modelPath + std::string("/iris_landmark.tflite"))
    {}


void Model::IrisLandmark::runInference() {
    FaceLandmark::runInference();
    auto roi = FaceDetection::getFaceRoi();
    if (roi.empty()) return;

    std::thread t([this]() {this->runEyeInference(true);});
    runEyeInference(false);
    t.join();
}


std::vector<cv::Vec3f> Model::IrisLandmark::getAllLandmarks() const {
    if (FaceDetection::getFaceRoi().empty())
        return std::vector<cv::Vec3f>();

    auto face_landmarks = FaceLandmark::getAllFaceLandmarks();
    auto left_iris_landmarks = getAllEyeLandmarks(true, true);
    auto right_iris_landmarks = getAllEyeLandmarks(false, true);

    std::vector<cv::Vec3f> landmarks(FACE_LANDMARKS + IRIS_LANDMARKS * 2);
    for (int i = 0; i < FACE_LANDMARKS; ++i) {
        landmarks[i] = face_landmarks[i];
    }
    for (int i = 0; i < IRIS_LANDMARKS; ++i) {
        landmarks[FACE_LANDMARKS + i] = left_iris_landmarks[i];
    }
    for (int i = 0; i < IRIS_LANDMARKS; ++i) {
        landmarks[FACE_LANDMARKS + IRIS_LANDMARKS + i] = right_iris_landmarks[i];
    }
    return landmarks;
}


cv::Vec3f Model::IrisLandmark::getEyeLandmarkAt(int index, bool isLeftEye, bool isIris) const {
    if (__isEyeIndexValid(index)) {
        auto model = isLeftEye ? &m_leftIrisLandmarker : &m_rightIrisLandmarker;
        auto eyeRoi = isLeftEye ? m_leftEyeRoi : m_rightEyeRoi;

        float _x = model->getOutputData(isIris)[index * 3];
        float _y = model->getOutputData(isIris)[index * 3 + 1];
        float _z = model->getOutputData(true)[index * 3 + 2];

        int x = (int) (_x / model->getInputShape()[2] * eyeRoi.width) + eyeRoi.x;
        int y = (int) (_y / model->getInputShape()[1] * eyeRoi.height) + eyeRoi.y;
        int z = _z;

        return cv::Vec3f(x, y, z);
    } else {
        return cv::Vec3f(0,0,0);
    }
}

std::vector<cv::Vec3f> Model::IrisLandmark::getAllEyeLandmarks(bool isLeftEye, bool isIris) const {
    if (Model::FaceDetection::getFaceRoi().empty())
        return std::vector<cv::Vec3f>();

    int n = isIris ? IRIS_LANDMARKS : EYE_LANDMARKS;

    std::vector<cv::Vec3f> landmarks(n);
    for (int i = 0; i < n; ++i) {
        landmarks[i] = getEyeLandmarkAt(i, isLeftEye, isIris);
    }
    return landmarks;
}


std::vector<float> Model::IrisLandmark::loadOutput(int index, bool isLeftEye) const {
    auto model = isLeftEye ? &m_leftIrisLandmarker: &m_rightIrisLandmarker;
    return model->loadOutput();
}


cv::Rect Model::IrisLandmark::getEyeRoi(bool isLeftEye) const {
    return isLeftEye ? m_leftEyeRoi: m_rightEyeRoi;
}

//-------------------Private methods start here-------------------

cv::Rect Model::IrisLandmark::calculateEyeRoi(cv::Point leftMoft, cv::Point rightMost) const{
    int cx = (leftMoft.x + rightMost.x) / 2;
    int cy = (leftMoft.y + rightMost.y) / 2; 

    int w = std::abs(leftMoft.x - rightMost.x);
    int h = std::abs(leftMoft.y - rightMost.y);
    w = h = std::max(w, h);
    
    return cv::Rect(cx - w/2, cy - h/2, w, h);
}


void Model::IrisLandmark::runEyeInference(bool isLeftEye) {
    int idx1 = isLeftEye ? 446 : 244;
    int idx2 = isLeftEye ? 464 : 226;

    auto roi = isLeftEye ? &m_leftEyeRoi: &m_rightEyeRoi;
    auto model = isLeftEye ? &m_leftIrisLandmarker: &m_rightIrisLandmarker;

    cv::Point pt1(static_cast<int>(FaceLandmark::getFaceLandmarkAt(idx1)[0]),
                  static_cast<int>(FaceLandmark::getFaceLandmarkAt(idx1)[1]));
    cv::Point pt2(static_cast<int>(FaceLandmark::getFaceLandmarkAt(idx2)[0]),
                  static_cast<int>(FaceLandmark::getFaceLandmarkAt(idx2)[1]));

    *roi = calculateEyeRoi(pt1, pt2);
    auto eyePatch = FaceDetection::cropFrame(*roi);

    model->loadImageToInput(eyePatch);
    model->runInference();
}