//
// Created by victor on 2-12-23.
//

#ifndef FACEMESHCPP_CAMERASTUFF_H
#define FACEMESHCPP_CAMERASTUFF_H

#ifdef PC_PLATFORM
#include <opencv2/highgui.hpp>
#else

#include <opencv2/highgui.hpp>
#include <RPI/LibCamera.h>

#endif


class UniversalCamera {
public:

    void init(int index, uint32_t width, uint32_t height, uint32_t frame_rate);

    bool get_frame(cv::Mat *frame);

    void reset_framebuffer();

    void stop();

private:
    uint32_t _width;
    uint32_t _height;
#if PC_PLATFORM
    std::unique_ptr<cv::VideoCapture> Cap;
#else
    LibcameraOutData frameData;
    uint32_t stride;
    LibCamera cam;
#endif
};

#endif //FACEMESHCPP_CAMERASTUFF_H