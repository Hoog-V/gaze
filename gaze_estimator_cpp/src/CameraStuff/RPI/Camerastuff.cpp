#include "CameraStuff.h"
#include "LibCamera.h"

void UniversalCamera::init(int index, const uint32_t width, const uint32_t height, const uint32_t frame_rate) {
    _width = width;
    _height = height;
    int ret = cam.initCamera(index);
    if (ret)
        throw "ERROR: Initializing camera failed!";
    cam.configureStill(width, height, formats::RGB888, 1, 0);
    int64_t frame_time = 1000000 / frame_rate;
    ControlList controls_;
    // Set frame rate
    controls_.set(controls::FrameDurationLimits, libcamera::Span<const int64_t, 2>({frame_time, frame_time}));
    // Adjust the brightness of the output images, in the range -1.0 to 1.0
    controls_.set(controls::Brightness, 0.6);
    // Adjust the contrast of the output image, where 1.0 = normal contrast
    controls_.set(controls::Contrast, 1.0);
    // Set the exposure time
    controls_.set(controls::ExposureTime, 20000);
    cam.set(controls_);
    cam.startCamera();
    cam.VideoStream(&_width, &_height, &stride);
}

bool UniversalCamera::get_frame(cv::Mat *frame) {
    bool skipframe = !cam.readFrame(&frameData);
    *frame = cv::Mat(_height, _width, CV_8UC3, frameData.imageData, stride);
    return skipframe;
}


void UniversalCamera::reset_framebuffer() {
    cam.returnFrameBuffer(frameData);
}

void UniversalCamera::stop() {
    cam.stopCamera();
    cam.closeCamera();
}