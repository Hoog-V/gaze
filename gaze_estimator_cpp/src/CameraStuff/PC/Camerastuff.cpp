#include "CameraStuff.h"

void UniversalCamera::init(int index, const uint32_t width, const uint32_t height, const uint32_t frame_rate) {
    Cap = std::make_unique<cv::VideoCapture>(index);
    if(Cap->isOpened() == false) {
        throw std::runtime_error("Cannot open the camera!");
    }
    Cap->set(cv::CAP_PROP_FRAME_WIDTH, width);
    Cap->set(cv::CAP_PROP_FRAME_HEIGHT, height);
    Cap->set(cv::CAP_PROP_FRAME_COUNT, frame_rate);
}

bool UniversalCamera::get_frame(cv::Mat *frame) {
    bool success = !Cap->read(*frame);
    return success;
}


void UniversalCamera::reset_framebuffer() {}

void UniversalCamera::stop() {
    Cap->release();
}