#ifndef GAZE_TRACKER_LANDMARKSMOOTHER_H
#define GAZE_TRACKER_LANDMARKSMOOTHER_H

#include <opencv2/core.hpp>
#include "Biquad.h"

// Filter coefficients for Biquad filters (lowpass).
// Generated using scipy.signal.butter with N=2 and Wn=0.25.
//const std::vector<std::vector<float>> sos = {{0.09763107, 0.19526215, 0.09763107, 1., -0.94280904, 0.33333333}};
const std::vector<std::vector<float>> sos = {{0.02008337, 0.04016673, 0.02008337, 1., -1.56101808, 0.64135154}};

/**
 * @brief A class for smoothing landmarks using a cascade of Biquad filters.
 *
 * The LandmarkSmoother applies Biquad filters to smooth the coordinates of facial landmarks.
 * The number of landmarks and the filter coefficients are configurable.
 *
 * Scipy can be used to generate the filter coefficients:
 * from scipy import signal
 * sos = signal.butter(N=2, Wn=0.1, btype='lowpass', output='sos')
 * static const std::vector<std::vector<float>> sos = {{0.02008337, 0.04016673, 0.02008337, 1., -1.56101808, 0.64135154}};
 * sos = signal.butter(N=2, Wn=0.25, btype='lowpass', output='sos')
 * static const std::vector<std::vector<float>> sos = {{0.09763107, 0.19526215, 0.09763107, 1., -0.94280904, 0.33333333}};
 */
class LandmarkSmoother {

public:
    /**
     * @brief Constructs a LandmarkSmoother with a specified number of landmarks.
     *
     * @param N The number of landmarks to be smoothed.
     */
    LandmarkSmoother(int N)
            : nr_of_landmarks(N), filters(3, std::vector<Biquads>(N, Biquads(sos))) {
    }

    /**
     * @brief Processes and smoothes a vector of 2D landmarks using Biquad filters.
     *
     *   @param landmarks A vector of 2D landmarks to be smoothed in-place.
     */
    void process(std::vector<cv::Vec3f> &landmarks);

private:
    int nr_of_landmarks; ///< The number of landmarks to be smoothed.
    std::vector<std::vector<Biquads>> filters; ///< A vector of Biquad filters for each landmark.
};

#endif //GAZE_TRACKER_LANDMARKSMOOTHER_H
