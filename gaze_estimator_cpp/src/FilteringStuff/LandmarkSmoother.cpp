#include "LandmarkSmoother.h"

void LandmarkSmoother::process(std::vector<cv::Vec3f> &landmarks) {
    if (landmarks.size() != nr_of_landmarks) {
        throw std::invalid_argument("wrong nr of landmarks");
    }
    for (int i = 0; i < nr_of_landmarks; i++) {
        for (int j = 0; j < 3; j++) {
            landmarks[i][j] = filters[j][i].process(landmarks[i][j]);
        }
    }
}
