#ifndef GAZE_TRACKER_BIQUAD_H
#define GAZE_TRACKER_BIQUAD_H

#include <vector>

/**
 * @brief A class representing a second-order digital filter using biquad transfer function.
 *
 * The filter is initialized with a set of second-order section (SOS) coefficients.
 * It supports processing a single input sample at a time.
 */

class Biquad {
public:
    /**
     * @brief Constructs a Biquad filter with given SOS coefficients.
     *
     * @param sos A vector containing six SOS coefficients [b0, b1, b2, a0, a1, a2].
     *
     * @throw std::invalid_argument if the number of coefficients is not equal to 6 or if a0 is not equal to 1.0.
     */
    Biquad(const std::vector<float> &sos);

    /**
     * @brief Processes a single input sample through the biquad filter.
     *
     * @param in The input sample to be processed.
     *
     * @return The filtered output sample.
     */
    ~Biquad() {};

    float process(const float in);

private:
    float b[3]; ///< Numerator coefficients [b0, b1, b2].
    float a[3]; ///< Denominator coefficients [1, a1, a2].
    float s[2] = {0}; ///< State variables [s1, s2].
    bool initialized = false; ///< Flag indicating whether the filter has been initialized.
};

/**
 * @brief A class representing a cascade of Biquad filters.
 *
 * The cascade is constructed with a vector of SOS coefficients for each section.
 * It supports processing a single input sample at a time through all sections.
 */
class Biquads {
public:
    /**
     * @brief Constructs a cascade of Biquad filters with given SOS coefficients for each section.
     *
     * @param sos A vector of vectors, each containing six SOS coefficients for a Biquad section.
     */
    Biquads(const std::vector<std::vector<float>> &sos);

    /**
     * @brief Processes a single input sample through the cascade of Biquad filters.
     *
     * @param in The input sample to be processed.
     *
     * @return The filtered output sample after passing through all sections.
     */
    ~Biquads() {};

    float process(const float in);

private:
    int nr_of_sections; ///< Number of Biquad sections in the cascade.
    std::vector<Biquad> cascade; ///< Vector of Biquad sections.
};


#endif //GAZE_TRACKER_BIQUAD_H
