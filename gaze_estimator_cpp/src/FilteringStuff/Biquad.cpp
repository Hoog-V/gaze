#include "Biquad.h"
#include <stdexcept>

Biquad::Biquad(const std::vector<float> &sos) {
    if (sos.size() != 6) {
        throw std::invalid_argument("Invalid number of coefficients");
    }
    for (int i = 0; i < 3; i++) {
        b[i] = sos[i];
        a[i] = sos[i + 3];
    }
    if (a[0] != 1.0) {
        throw std::invalid_argument("First feedback coefficient (a_0) is assumed to be equal to 1");
    }
}

float Biquad::process(const float in) {
    float out;
    if (initialized) {
        out = b[0] * in + s[0];
        s[0] = s[1] + b[1] * in - a[1] * out;
        s[1] = b[2] * in - a[2] * out;
    } else {
        out = in * (b[0] + b[1] + b[2]) / (1 + a[1] + a[2]);
        s[1] = b[2] * in - a[2] * out;
        s[0] = s[1] + b[1] * in - a[1] * out;
        initialized = true;
    }
    return out;
}

Biquads::Biquads(const std::vector<std::vector<float>> &sos) {
    nr_of_sections = sos.size();
    for (int i = 0; i < nr_of_sections; i++) {
        cascade.push_back(Biquad(sos[i]));
    }
}

float Biquads::process(const float in) {
    float out = in;

    for (int i = 0; i < nr_of_sections; i++) {
        out = cascade[i].process(out);
    }
    return out;
}