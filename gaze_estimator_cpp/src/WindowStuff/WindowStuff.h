//
// Created by victor on 2-12-23.
//

#ifndef FACEMESHCPP_WINDOWSTUFF_H
#define FACEMESHCPP_WINDOWSTUFF_H

#include <stdint.h>
#include <opencv2/highgui.hpp>
#include "FPSManager.h"
class WindowManager {
public:
    void setup(const char *name, uint32_t width, uint32_t height, bool fullscreen);

    void setup(const char *name, uint32_t width, uint32_t height, FPSManager* FPSManager_inst, bool fullscreen);

    int update(cv::Mat frame);
private:
bool _fps_counter_enabled = false;
const char* _window_name;
FPSManager* _fps_manager_inst = NULL;
};



#endif //FACEMESHCPP_WINDOWSTUFF_H
