//
// Created by victor on 2-12-23.
//

#ifndef FACEMESHCPP_FPSMANAGER_H
#define FACEMESHCPP_FPSMANAGER_H
#include <chrono>

class FPSManager {
public:
    void ResetFPSCounter() {
    start = std::chrono::high_resolution_clock::now();
    }

    const int getFPS() {
        stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        float inferenceTime = duration.count() / 1e3;
        sum += inferenceTime;
        count += 1;
        return (int) 1e3/ inferenceTime;
    }
private:
    float sum = 0;
    int count = 0;
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    std::chrono::time_point<std::chrono::high_resolution_clock> stop;
};

#endif //FACEMESHCPP_FPSMANAGER_H
