//
// Created by victor on 2-12-23.
//
#include <opencv2/imgproc.hpp>
#include "WindowStuff.h"

void WindowManager::setup(const char *name, uint32_t width, uint32_t height, bool fullscreen) {
    cv::namedWindow(name, cv::WINDOW_NORMAL);
    cv::resizeWindow(name, width, height);
    if (fullscreen) {
        cv::setWindowProperty(name, cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
    }
    _window_name = name;
    _fps_counter_enabled = false;
}

void WindowManager::setup(const char *name, uint32_t width, uint32_t height, FPSManager *FPSManager_inst,
                                 bool fullscreen) {
    if (FPSManager_inst == NULL) {
        throw "ERROR: Invalid FPSManager instance";
    }
    cv::namedWindow(name, cv::WINDOW_NORMAL);
    if (fullscreen) {
        cv::setWindowProperty(name, cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
    }
    _fps_manager_inst = FPSManager_inst;
    _fps_counter_enabled = true;
    _window_name = name;
}

int WindowManager::update(cv::Mat frame) {
    if (_fps_counter_enabled) {
        const int fps = _fps_manager_inst->getFPS();
        cv::putText(frame, std::to_string(fps), cv::Point(20, 70), cv::FONT_HERSHEY_PLAIN, 3, cv::Scalar(0, 196, 255), 2);
    }
    cv::imshow(_window_name, frame);
    return cv::pollKey();
}