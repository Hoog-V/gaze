//
// Created by victor on 16-1-24.
//

#ifndef GAZE_TRACKER_DRAWSTUFF_H
#define GAZE_TRACKER_DRAWSTUFF_H
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

void drawReference(const cv::Mat& frame, const cv::Vec3f& eulerAngles, const cv::Vec3f& translation,
                   const float arrowLength = 50.0, const cv::Scalar& color = cv::Scalar(255, 0, 0)) {
    double pitch = eulerAngles[0];
    double yaw = eulerAngles[1];
    double roll = eulerAngles[2];
    auto origin = cv::Point2f (translation[0], translation[1]);
    cv::Point2f xAxis(static_cast<float>(cos(yaw) * cos(roll)),
                      static_cast<float>(-sin(roll) * cos(pitch) + cos(roll) * sin(pitch) * sin(yaw)));
    cv::Point2f yAxis(static_cast<float>(cos(yaw) * sin(roll)),
                      static_cast<float>(cos(pitch) * cos(roll) + sin(pitch) * sin(roll) * sin(yaw)));
    cv::Point2f zAxis(static_cast<float>(sin(yaw)),
                      static_cast<float>(-cos(yaw) * sin(pitch)));

    xAxis *= arrowLength;
    yAxis *= arrowLength;
    zAxis *= arrowLength;
    cv::arrowedLine(frame, origin, origin + xAxis, color, 2);
    cv::arrowedLine(frame, origin, origin + yAxis, color, 2);
    cv::arrowedLine(frame, origin, origin + zAxis, color, 2);
}

void drawLandmarks(cv::Mat& frame, const std::vector<cv::Vec3f>& landmarks,
                   const cv::Scalar& color = cv::Scalar(0, 255, 0)) {
    for (auto landmark: landmarks) {
        cv::circle(frame, cv::Point (landmark[0], landmark[1]), 2, color, -1);
    }
}


#endif //GAZE_TRACKER_DRAWSTUFF_H
