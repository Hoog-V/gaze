#include <iostream>
#include <thread>
#include <opencv2/highgui.hpp>
#include <boost/asio.hpp>
#include "WindowStuff/WindowStuff.h"
#include "CameraStuff.h"
#include "IrisLandmark.hpp"
#include "ProcessingStuff/head_estimation.h"
#include "ProcessingStuff/gaze_estimation.h"
#include "ProcessingStuff/MeshAnnotations.h"
#include "FilteringStuff/LandmarkSmoother.h"
#include "WindowStuff/DrawStuff.h"
#include "CommunicationStuff/tcp_server.h"

#define NR_OF_IRIS_LANDMARKS 5
#define NR_OF_FACE_LANDMARKS 468
#define NR_OF_LANDMARKS (NR_OF_FACE_LANDMARKS + 2 * NR_OF_IRIS_LANDMARKS) // face and iris landmarks combined

/* This macro converts radians to degrees */
#define radtodeg(rad) ((rad) * (180/CV_PI))

#define ESCAPE_KEY 27

/*
 * This option enables landmark smoothing, it applies a Lowpass filter to the incoming landmark points
 * It reduces jitter at the cost of response rate
 */
#define SMOOTHING 1

using boost::asio::ip::tcp;

static inline constexpr char window_name[] = "Gaze detector";

int main(int argc, char *argv[]) {
    UniversalCamera camera;

    /* Load the model in to memory and tflite */
    Model::IrisLandmark irisLandmarker("../models");

    WindowManager window;
    FPSManager FPSCounter;

    cv::Mat frame, rframe;
    cv::Vec3f headRotation, headTranslation, gazeDirection;

    calculated_angles Angle;
    Results results;
    namespace bip = boost::interprocess;
    bip::message_queue::remove("message_queue");
//    bip::message_queue mq(bip::create_only, "message_queue", 20, sizeof(calculated_angles));
    bip::message_queue mq(bip::create_only, "message_queue", 20, sizeof(results));

    std::thread tcp_server(tcp_server_thread);

    const int midwayBetweenEyesCoordinates = face_mesh_annotations[midwayBetweenEyes][0];
    const int leftIrisCenterCoordinates = face_mesh_annotations[leftEyeIris][0];
    const int rightIrisCenterCoordinates = face_mesh_annotations[rightEyeIris][0];

#if SMOOTHING
    LandmarkSmoother landmarkSmoother(NR_OF_LANDMARKS);
#endif
    /* The index when using multiple camera's,
     * use zero for the default camera
     */
    const uint8_t camera_index = 0;
    const uint16_t camera_fps = 60;
    const uint32_t width = 640;
    const uint32_t height = 480;

    camera.init(camera_index, width, height, camera_fps);

    window.setup(window_name, width, height, &FPSCounter, false);

    int pressed_key;
    int results_buffer_index = 0;

    while (true) {


        bool camera_frame_invalid = camera.get_frame(&rframe);

        if(camera_frame_invalid) {
            continue;
        }

        /* Reset FPSCounter */
        FPSCounter.ResetFPSCounter();

        const uint8_t flip_code = 1;
        cv::flip(rframe, frame, flip_code);

        irisLandmarker.loadImageToInput(frame);
        irisLandmarker.runInference();

        auto landmarks = irisLandmarker.getAllLandmarks();

        results.faceDetectedFlag = false;
        results.headTranslation = (0,0,0);

        if (!landmarks.empty()) {

#if SMOOTHING
            landmarkSmoother.process(landmarks);
#endif
            Angle.head_angle = head_angle_estimation(landmarks);
            headTranslation = landmarks[midwayBetweenEyesCoordinates];
            Angle.gaze_angle = gaze_direction_estimation(landmarks);
            float distance = calculateCameraDistance(landmarks, width);

            std::cout << "Distance: " << distance << " m" << std::endl;
//TODO: distance estimation

            /* Draw the face landmarks and head rotation (yaw, roll and pitch) for the preview window */
            const cv::Scalar landmark_color(0, 255, 0);
            drawLandmarks(frame, landmarks, landmark_color);
            drawReference(frame, Angle.head_angle, headTranslation);

            /* Draw the gaze rotation (yaw, roll and pitch) for the preview window */
            const float gaze_arrow_length = 10.0f;
            const cv::Scalar gaze_arrow_color(0, 255, 255);
            drawReference(frame, Angle.gaze_angle, landmarks[leftIrisCenterCoordinates], gaze_arrow_length, gaze_arrow_color);
            drawReference(frame, Angle.gaze_angle, landmarks[rightIrisCenterCoordinates], gaze_arrow_length, gaze_arrow_color);

            Angle.head_angle[0] = radtodeg(Angle.head_angle[0]);
            Angle.head_angle[1] = radtodeg(Angle.head_angle[1]);
            Angle.head_angle[2] = radtodeg(Angle.head_angle[2]);

            Angle.gaze_angle[0] = radtodeg(Angle.gaze_angle[0]);
            Angle.gaze_angle[1] = radtodeg(Angle.gaze_angle[1]);
            Angle.gaze_angle[2] = radtodeg(Angle.gaze_angle[2]);


            results.faceDetectedFlag = TRUE;
            results.headTranslation[0] = headTranslation[0]; // [px]
            results.headTranslation[1] = headTranslation[1]; // [px]
            results.headTranslation[2] = distance; // [m]

//            try{
//            mq.try_send(&Angle, sizeof(Angle), 0);
//            } catch (boost::interprocess::interprocess_exception &ex) {
//                std::cerr << "Interprocess Exception: " << ex.what() << std::endl;
//                return 1;
//            }

        }
        /* Update the preview window */
        pressed_key = window.update(frame);

        /*
         * Resets the framebuffer; when using USB webcam, nothing happens.
         * Though, when using LibCamera with the RPI, it frees the previous allocated framebuffer.
         * The LibCamera driver needs a rewrite.
         * As it captures still images from the sensor instead of requesting a stream of images.
         */
        camera.reset_framebuffer();

        /* Quit when the ESC key is pressed */
        if(pressed_key == ESCAPE_KEY) {
            break;
        }

        try{
            mq.try_send(&results, sizeof(Results), 0);
        } catch (boost::interprocess::interprocess_exception &ex) {
            std::cerr << "Interprocess Exception: " << ex.what() << std::endl;
            return 1;
        }

    }
    cv::destroyAllWindows();
    camera.stop();
    tcp_server.detach();
    return 0;
}
