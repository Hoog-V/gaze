# C++ Gaze tracker

This is a C++ application which utilizes the MediaPipe models with TensorFlow Lite to do gaze tracking.

Besides being independent of the MediaPipe Framework this application is written to be portable/usable among different
platforms. 

For more information:
* Face detection: https://google.github.io/mediapipe/solutions/face_detection.html
* Face landmarks: https://google.github.io/mediapipe/solutions/face_mesh.html
* Iris landmarks: https://google.github.io/mediapipe/solutions/iris.html

## :warning: Why not using GPU ?
Because GPU support has launched very recently (for non-mobile applications) and is still very experimental. Eventually there are plans to move some of the processor intensive tasks to the GPU, but for now the program will only use the CPU. 

## :computer: Requirements:

### Hardware: 
This program can be ran on Linux, Mac and Windows 10+. 

To be able to use and build this program you need: 8GB+ of Disk space and >4GB of RAM.


**This program has Raspberry Pi support. Though it is highly suggested to use a model newer than 3B with enough memory >4GB.**

Expect performance numbers ranging from 25-30FPS with a camera resolution of 800x600 running on the CPU (tested with Raspberry Pi 4B 8GB).


**Please note that when using a Raspberry Pi, CMake will automatically link in the Libcamera library to be used with the Picamera. If you want to use a USB webcam with the native OpenCV VideoCapture class, you have to use the Compile definition: `-DPC_PLATFORM` as argument when generating with CMake.**


## Development environment setup

See the [cross compile guide](docs/cross_compiling_rpi.md) for the installation of the cross-compilation (Raspberry Pi) environment.

See the [native build guide](docs/native_build.md) for the installation of the native build environment on Windows and Linux.


#### Tensorflow Lite
<details>
  <summary>How to use the submodule</summary>
  In this repository TensorFlow is added as a submodule. This submodule has to be initialized when you want to build this application.
  
  **Note! You can skip this step al together when you have either already initialized the submodule or recursively cloned this repo**

  1. Initializing can be done by running this command with the workpath of your terminal in the root of this git repository.
    `git submodule update --init`
  2. Wait for the command to finish as the tensorflow library is quite large >800Mb :) 
</details>
